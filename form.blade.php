@extends('crudbooster::admin_template')
@section('content')
<div> <!-- main of form.blade.php -->

	@if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
		@if(g('return_url'))
			<p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
					&nbsp; {{cbLang("form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
		@else
			<p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
					&nbsp; {{cbLang("form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
		@endif
	@endif

	<div class="panel panel-default">
		<div class="panel-heading">
			<!--<strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title !!}</strong> -->
		   <a data-toggle="collapse" href="#{{CRUDBooster::getCurrentModule()->table_name}}-collapse1"> <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title !!}</strong></a> 
			
		</div>
		<!-- forma.blade.php panel-heading -->
		<div id="{{CRUDBooster::getCurrentModule()->table_name}}-collapse1" class="panel-collapse collapse.show"> <!-- collapse1 vilte-->
			<div class="panel-body" style="padding:20px 0px 0px 0px">
				<?php
				$action = (@$row) ? CRUDBooster::mainpath("edit-save/$row->id") : CRUDBooster::mainpath("add-save");
				$return_url = ($return_url) ?: g('return_url');
				?>
				<form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action='{{$action}}'>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type='hidden' name='return_url' value='{{ @$return_url }}'/>
					<input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
					<input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
					@if($hide_form)
						<input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
					@endif
					<div class="box-body" id="parent-form-area">
						@if($command == 'detail')
							@include("crudbooster::default.form_detail")
						@else
							@include("crudbooster::default.form_body")			
						@endif
					</div>
					<!-- form.blade.php .box-body parent-form-area-->
					<div class="box-footer" style="background: #F5F5F5">
						<div class="form-group">
							<label class="control-label col-sm-2"></label>
							<div class="col-sm-10">
								@if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
									@if(g('return_url'))
										<a href='{{g("return_url")}}' class='btn btn-default'>
											<i class='fa fa-chevron-circle-left'></i> {{cbLang("button_back")}}</a>
									@else
										<a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'>
											<i class='fa fa-chevron-circle-left'></i> {{cbLang("button_back")}}</a>
									@endif
								@endif
								@if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())
									@if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
										<input type="submit" name="submit" value='{{cbLang("button_save_more")}}' class='btn btn-success'>
									@endif

									@if($button_save && $command != 'detail')
										<input type="submit" name="submit" value='{{cbLang("button_save")}}' class='btn btn-success'>
									@endif

								@endif
							</div>
							<!--form.blade.php .col-sm-10-->
						</div>
						<!--form.blade.php .form-group -->
					</div>
					<!--form.blade.php .box-footer-->
				</form>
				<!--form.blade.php .form-horizontal-->

			</div>
			<!--form.blade.php .panel body-->
		</div>
		<!--form.blade.php .panel-collapse collapse.show vilte-->
	</div>
	<!--form.blade.php .panel panel-default-->
</div>
<!--form.blade.php END AUTO MARGIN vilte-->
@endsection
